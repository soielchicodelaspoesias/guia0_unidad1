#include <iostream>
using namespace std;
#include "Coordenadas.h"

/* constructores */
Coordenadas::Coordenadas() {
    float x = 0;
    float y = 0;
    float z = 0;
}

Coordenadas::Coordenadas (float x, float y, float z) {
    this->x = x;
    this->y = y;
    this->z = z;
}

/* métodos get and set */
float Coordenadas::get_x() {
    return this->x;
}

float Coordenadas::get_y() {
    return this->y;
}

float Coordenadas::get_z(){
  return this->z;
}

void Coordenadas::set_x(float x) {
    this->x = x;
}

void Coordenadas::set_y(float y) {
    this->y = y;
}

void Coordenadas::set_z(float z) {
    this->z = z;
}
