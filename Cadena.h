#include <iostream>
#include <list>
#include "Aminoacidos.h"
using namespace std;

#ifndef CADENA_H
#define CADENA_H

class Cadena {
    private:
      string letra = "";
      list<Aminoacido> aminoacidos;

    public:
        Cadena ();
        Cadena (string letra);
        string get_letra();
        void set_letra(string letra);
        list<Aminoacido> get_aminoacido();
        list<Aminoacido> add_aminoacido(list<Aminoacido> aminoacidos);

};
#endif
