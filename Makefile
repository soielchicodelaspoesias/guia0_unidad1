prefix=/usr/local
CC = g++

CFLAGS = -g -Wall
SRC = programa.cpp proteina.cpp cadena.cpp aminoacido.cpp atomo.cpp coordenadas.cpp
OBJ = programa.o proteina.o cadena.o aminoacido.o atomo.o coordenadas.o
APP = programa

all: $(OBJ)
	$(CC) $(CFLAGS) -o $(APP) $(OBJ)

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
