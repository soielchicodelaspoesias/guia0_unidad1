#include <list>
#include <iostream>
#include <cstdlib>
#include "Proteina.h"

using namespace std;
// Clase donde se crea la lista
class Lista {
    // Se instancia las variables privadas
    public:
      list <Proteina> lista_proteinas;


  list <Proteina> get_lista(){
    return this->lista_proteinas;
  }

};

void imprimir_proteinas(list <Proteina> lista_proteinas){
    // for que imprime el nombre y la id de la proteina
    for (Proteina p: lista_proteinas) {
      cout << "--------------------------------------" << '\n';
      cout << "Nombre: " << p.get_nombre_proteina() << endl;
      cout << "Id: "<< p.get_id() << endl;
      // se imprime la letra de la cadena ingresada por el usuario
      for(Cadena c: p.get_cadena()){
        cout << "Cadena: "<< c.get_letra()<< endl;
         // de aqui en adelante no logre que me imprimiera las cosas, esta igual que las demas pero no pasa nada
          for(Aminoacido a: c.get_aminoacido()){
                cout << "Nombre del aminoacido: "<< a.get_nombre_aminoacido() << endl;
                cout << "Numero del aminoacido: " << a.get_numero_aminoacido() << endl;
                for(Atomo at: a.get_atomos()){
                  cout << "Nombre del atomo: "<< at.get_nombre_atomo() << endl;
                  cout << "Numero del atomo: " << at.get_numero_atomo() << endl;
                  //for(Coordenadas co){
                  //  cout << "Coordenada x: "<< co.get_x() << endl;
                  //  cout << "Coordenada y: " << co.get_y() << endl;
                  //  cout << "Coordenada z: " << co.get_z() << endl;
                  //}
              }
          }
      }
   }
}


// funcion para agregar proteinas
list <Proteina> agregar_proteina (list <Proteina> lista_proteinas) {
  string line;
  string line2;
  Cadena c = Cadena();
  Aminoacido am = Aminoacido();
  Proteina p = Proteina();
  cout << "Nombre: ";
  cin >> line;
  cout << "Id: ";
  cin >> line2;
  p.set_nombre_proteina(line);
  p.set_id(line2);
  p.add_cadena();
  lista_proteinas.push_back(p);

  return lista_proteinas;
}

int main () {
  int opcion;
  Proteina();
  Lista l = Lista();
  list <Proteina> lista_proteinas = l.get_lista();
  // MENU
  do {
    cout << "MENU" << endl;
    cout << "1.- Agregar proteina" << endl;
    cout << "2.- Ver listado de proteinas" << endl;
    cout << "3.- Salir" << endl;

    cout << "Ingrese una opcion: ";
    cin >> opcion;

    switch(opcion){
      case 1:
          lista_proteinas = agregar_proteina(lista_proteinas);
          system("clear");
          break;
      case 2:
          imprimir_proteinas(lista_proteinas);
          break;
      }
   }while (opcion != 3);
    return 0;
}
