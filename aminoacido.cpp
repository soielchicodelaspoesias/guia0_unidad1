#include <iostream>
#include <list>
using namespace std;
#include "Atomo.h"
#include "Aminoacidos.h"

/* constructores */
Aminoacido::Aminoacido() {
    string nombre_aminoacido = "";
    int numero_aminoacido = 0;
    list<Atomo> atomos;
}

Aminoacido::Aminoacido (string nombre_aminoacido, int numero_aminoacido) {
    this->nombre_aminoacido = nombre_aminoacido;
    this->numero_aminoacido = numero_aminoacido;
    this->atomos = atomos;
}

/* métodos get and set */
string Aminoacido::get_nombre_aminoacido() {
    return this->nombre_aminoacido;
}

int Aminoacido::get_numero_aminoacido() {
    return this->numero_aminoacido;
}

void Aminoacido::set_nombre_aminoacido(string nombre_aminoacido) {
    this->nombre_aminoacido = nombre_aminoacido;
}

void Aminoacido::set_numero_aminoacido(int numero_aminoacido) {
    this->numero_aminoacido = numero_aminoacido;
}

list<Atomo> Aminoacido::get_atomos(){
  return this-> atomos;
}
// Se agregan atomos y se llama a agregar coordenadas
list<Atomo> Aminoacido::add_atomos(){
  string nombre;
  int numero;
  Atomo a = Atomo();
  cout << "Nombre del atomo: ";
  cin >> nombre;
  cout << "Numero del atomo: ";
  cin >> numero;
  a.set_nombre_atomo(nombre);
  a.set_numero_atomo(numero);
  atomos.push_back(a);
  a.set_coordenadas();


  return atomos;
}
