#include <iostream>
#include <list>
#include "Cadena.h"
using namespace std;

#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina {
    private:
        string nombre_proteina = "";
        string id = "";
        list<Cadena> cadena;

    public:
        Proteina ();
        Proteina (string nombre_proteina, string id);
        string get_nombre_proteina();
        string get_id();
        list<Cadena> get_cadena();
        void set_nombre_proteina(string nombre_proteina);
        void set_id(string id);
        void add_cadena();
};
#endif
