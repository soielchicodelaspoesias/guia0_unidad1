#include <iostream>
#include "Coordenadas.h"
using namespace std;

#ifndef ATOMO_H
#define ATOMO_H

class Atomo {
    private:
      string nombre_atomo = "";
      int numero_atomo = 0;
      float x = 0;
      float y = 0;
      float z = 0;

    public:
        Atomo ();
        Atomo (string nombre_atomo, int numero_atomo);

        /* métodos get and set */
        string get_nombre_atomo();
        int get_numero_atomo();

        void set_nombre_atomo(string nombre_atomo);
        void set_numero_atomo(int numero_atomo);
        float get_coordenadas();
        void set_coordenadas();


};
#endif
