# guia0_unidad1

*Creador de proteínas*                                                                                             
(C.P)

Crea tu propia proteína con este programa                                                                        

*Pre-requisitos* 

C++
make                                                                                                                                                                                                                                           

*Ejecutando*                                                                                                                                         
Al abrir el programa se encontrará con un menu en el cual estaran 3 opciones: Agregar proteína, Ver listado de proteínas, Salir.
Al Agregar proteínas le va a pedir los siguientes datos: Nombre, id, cadena(nombre, numero), aminoacido(nombre, numero)
, atomo(nombre, numero) y coordenas(x, z, y)
Ver listado de proteínas: va a mostrar el listado de proteínas creadas

*Construido con*                                                                                                                                    
C++                                                                                                           

*Versionado*                                                                                                                                        
Version 0,1                                                                                                                                       

*Autor*                                                                                                                                           
Luis Rebolledo                                                                                                                                                                                                                                                                                                                        

