#include <iostream>
#include <list>
#include "Atomo.h"
using namespace std;

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

class Aminoacido {
    private:
      string nombre_aminoacido = "";
      int numero_aminoacido = 0;
      list<Atomo> atomos;

    public:
        /* constructores */
        Aminoacido ();
        Aminoacido (string nombre_aminoacido, int numero_aminoacido);
        string get_nombre_aminoacido();
        int get_numero_aminoacido();

        void set_nombre_aminoacido(string nombre_aminoacido);
        void set_numero_aminoacido(int numero_aminoacido);
        list<Atomo> get_atomos();
        list<Atomo> add_atomos();

};
#endif
